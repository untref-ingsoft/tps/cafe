Sequel.migration do
  up do
    add_column :job_offers, :applications, Integer, default: 0
  end

  down do
    drop_column :job_offers, :applications
  end
end
