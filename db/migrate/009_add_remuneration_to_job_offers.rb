Sequel.migration do
  up do
    add_column :job_offers, :remuneration, String
  end

  down do
    drop_column :job_offers, :remuneration
  end
end
