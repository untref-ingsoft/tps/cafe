class JobConsult
  include ActiveModel::Validations

  attr_accessor :applicant_email, :job_offer, :consult

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i.freeze

  validates :job_offer, :consult, presence: true

  validates :applicant_email, format: { presence: true, with: VALID_EMAIL_REGEX, message: 'Invalid Email' }

  def initialize(email, job_offer, consult)
    @applicant_email = email
    @job_offer = job_offer
    @consult = consult
    validate!
  end

  def self.create_for_consult(email, job_offer, consult)
    JobConsult.new(email, job_offer, consult)
  end

  def process
    JobVacancy::App.deliver(:notification, :contact_consult_email, self)
  end
end
