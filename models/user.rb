class User
  include ActiveModel::Validations

  attr_accessor :id, :name, :email, :crypted_password, :job_offers, :updated_on, :created_on, :password

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i.freeze

  validates :name, presence: true
  validates :email, presence: true, format: { with: VALID_EMAIL_REGEX,
                                              message: 'invalid email' }
  validate :validate_password

  def initialize(data = {})
    @id = data[:id]
    @name = data[:name]
    @email = data[:email]
    @password = data[:password]
    @crypted_password = if data[:password].nil?
                          data[:crypted_password]
                        else
                          Crypto.encrypt(data[:password])
                        end
    @job_offers = data[:job_offers]
    @updated_on = data[:updated_on]
    @created_on = data[:created_on]
  end

  def has_password?(password)
    Crypto.decrypt(crypted_password) == password
  end

  def validate_password # rubocop:disable Metrics/AbcSize
    flag_length = @password !~ /.{10,}/
    flag_lowercase = @password !~ /[a-z]+/
    flag_uppercase = @password !~ /[A-Z]+/
    flag_number = @password !~ /\d+/
    flag_special = @password !~ /\W+/
    flag_non_consecutive = @password.match(/(.)\1/)

    if flag_length
      errors.add(:password, 'should be at least 10 characters long')
      nil
    end

    if flag_lowercase
      errors.add(:password, 'should be at least one lowercase letter')
      nil
    end

    if flag_uppercase
      errors.add(:password, 'should be at least one uppercase letter')
      nil
    end

    if flag_number
      errors.add(:password, 'should have at least one number')
      nil
    end

    if flag_special
      errors.add(:password, 'should have at least one special character')
      nil
    end

    if flag_non_consecutive
      errors.add(:password, 'should not have repeating consecutive characters')
      nil
    end
  end
end
