JobVacancy::App.controllers :users do
  get :new, map: '/register' do
    @user = User.new
    render 'users/new'
  end

  post :create do
    password_confirmation = params[:user][:password_confirmation]
    params[:user].reject! { |k, _| k == 'password_confirmation' }

    @user = User.new(params[:user])

    if params[:user][:password] != password_confirmation
      flash.now[:error] = 'Passwords do not match'
      @user.password = ''
      render 'users/new'
    elsif !User.new(params[:user]).valid?
      @user.validate
      @user.password = ''
      flash.now[:error] = @user.errors.full_messages.join(', ')
      render 'users/new'
    elsif !verify_recaptcha
      @user.password = ''
      flash.now[:error] = 'Invalid captcha'
      render 'users/new'
    else
      UserRepository.new.save(@user)
      flash[:success] = 'User created'
      redirect '/'
    end
  end
end
