JobVacancy::App.controllers :job_offers do
  get :my do
    @offers = JobOfferRepository.new.find_by_owner(current_user)
    render 'job_offers/my_offers'
  end

  get :index do
    @offers = JobOfferRepository.new.all_active
    render 'job_offers/list'
  end

  get :new do
    @job_offer = JobOfferForm.new
    render 'job_offers/new'
  end

  get :latest do
    @offers = JobOfferRepository.new.all_active
    render 'job_offers/list'
  end

  post :contact, with: :offer_id do
    @job_offer = JobOfferRepository.new.find(params[:offer_id])
    applicant_email = params[:job_offer_form][:applicant_email]
    consult = params[:job_offer_form][:consult]
    @job_consult = JobConsult.create_for_consult(applicant_email, @job_offer, consult)
    if @job_consult
      @job_consult.process
      flash[:success] = 'Contact information sent.'
      redirect '/job_offers'
    end
  rescue ActiveModel::ValidationError => e
    @errors = e.model.errors
    flash.now[:error] = 'Please review the errors'
    render 'job_offers/contact_offer'
  end

  get :edit, with: :offer_id do
    @job_offer = JobOfferForm.from(JobOfferRepository.new.find(params[:offer_id]))
    # TODO: validate the current user is the owner of the offer
    render 'job_offers/edit'
  end

  get :apply, with: :offer_id do
    @job_offer = JobOfferForm.from(JobOfferRepository.new.find(params[:offer_id]))
    @offers = !@job_offer&.tags.nil? ? JobOfferRepository.new.find_suggestions_by_tags(@job_offer.tags) : []
    @offers_filter = @offers.reject { |off| off.id == params[:offer_id].to_i }
    @offers_filter_count = @offers_filter.take(3)
    @job_application = JobApplicationForm.new
    render 'job_offers/apply'
  end

  get :search do
    @offers = JobOfferRepository.new.search_by_title(params[:q])
    render 'job_offers/list'
  end

  get :contact, with: :offer_id do
    @job_offer = JobOfferForm.from(JobOfferRepository.new.find(params[:offer_id]))
    render 'job_offers/contact_offer'
  end

  post :apply, with: :offer_id do
    @job_offer = JobOfferRepository.new.find(params[:offer_id])
    applicant_email = params[:job_application_form][:applicant_email]
    @job_application = JobApplication.create_for(applicant_email, @job_offer)

    if verify_recaptcha
      @job_application.process
      @job_offer.applications = @job_offer.applications + 1
      if JobOfferRepository.new.save(@job_offer)
        flash[:success] = 'Contact information sent'
        redirect '/job_offers'
      else
        flash.now[:error] = 'Error sending email '
      end
    else
      flash[:error] = 'Invalid captcha'
      redirect_to "job_offers/apply/#{params[:offer_id]}"
    end
  rescue ActiveModel::ValidationError
    flash[:error] = 'Invalid email'
    redirect_to "job_offers/apply/#{params[:offer_id]}"
  end

  post :create do
    job_offer = JobOffer.new(job_offer_params)
    job_offer.owner = current_user
    if JobOfferRepository.new.save(job_offer)
      TwitterClient.publish(job_offer) if params['create_and_twit']
      flash[:success] = 'Offer created'
      redirect '/job_offers/my'
    end
  rescue ActiveModel::ValidationError => e
    @job_offer = JobOfferForm.new
    @errors = e.model.errors
    flash.now[:error] = 'Please review the errors'
    render 'job_offers/new'
  end

  post :update, with: :offer_id do
    @job_offer = JobOffer.new(job_offer_params.merge(id: params[:offer_id]))
    @job_offer.owner = current_user

    if JobOfferRepository.new.save(@job_offer)
      flash[:success] = 'Offer updated'
      redirect '/job_offers/my'
    end
  rescue ActiveModel::ValidationError => e
    @job_offer = JobOfferForm.from(JobOfferRepository.new.find(params[:offer_id]))
    @errors = e.model.errors
    flash.now[:error] = 'Please review the errors'
    render 'job_offers/edit'
  end

  put :activate, with: :offer_id do
    @job_offer = JobOfferRepository.new.find(params[:offer_id])
    @job_offer.activate
    if JobOfferRepository.new.save(@job_offer)
      flash[:success] = 'Offer activated'
    else
      flash.now[:error] = 'Operation failed'
    end

    redirect '/job_offers/my'
  end

  delete :destroy do
    @job_offer = JobOfferRepository.new.find(params[:offer_id])
    if JobOfferRepository.new.destroy(@job_offer)
      flash[:success] = 'Offer deleted'
    else
      flash.now[:error] = 'Title is mandatory'
    end
    redirect 'job_offers/my'
  end
end
