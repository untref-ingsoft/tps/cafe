require 'recaptcha'

module JobVacancy
  class App < Padrino::Application
    register Padrino::Rendering
    register Padrino::Mailer
    register Padrino::Helpers

    include Recaptcha::Adapters::ControllerMethods
    include Recaptcha::Adapters::ViewMethods

    enable :sessions

    Padrino.configure :development, :test do
      set :delivery_method, file: {
        location: "#{Padrino.root}/tmp/emails"
      }

      Recaptcha.configure do |config|
        config.site_key = '6LfoKfccAAAAANZ_sXifFULe-XHJhY4CLguXkb_O'
        config.secret_key = '6LfoKfccAAAAADiSXOBMTvHYj1V7hMcsaUaaOuD1'
      end
    end

    Padrino.configure :staging, :production do
      set :delivery_method, smtp: {
        address: ENV['SMTP_ADDRESS'],
        port: ENV['SMTP_PORT'],
        user_name: ENV['SMTP_USER'],
        password: ENV['SMTP_PASS'],
        authentication: :plain,
        enable_starttls_auto: true
      }

      Recaptcha.configure do |config|
        config.site_key = ENV['RECAPTCHA_PUBLIC_KEY']
        config.secret_key = ENV['RECAPTCHA_PRIVATE_KEY']
      end
    end
  end
end
