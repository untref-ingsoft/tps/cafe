class JobOfferForm
  attr_accessor :id, :title, :location, :description, :tags

  def self.from(a_job_offer)
    form = JobOfferForm.new
    form.id = a_job_offer.id
    form.title = a_job_offer.title
    form.location = a_job_offer.location
    form.description = a_job_offer.description
    form.tags = a_job_offer.tags&.join(', ')
    form
  end
end
