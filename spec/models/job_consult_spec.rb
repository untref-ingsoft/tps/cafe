require 'spec_helper'

describe JobConsult do
  let(:job_offer) { JobOffer.new(title: 'a title') }

  describe 'create_for_consult' do
    it 'should set applicant_email' do
      email = 'test@test.com'
      consult = 'how many working hours are per week?'
      ja = described_class.create_for_consult(email, job_offer, consult)
      expect(ja.applicant_email).to eq(email)
    end

    it 'should set job_offer' do
      offer = job_offer
      ja = described_class.create_for_consult('test@test.com', offer, 'how many working hours are per week?')
      expect(ja.job_offer).to eq(job_offer)
    end

    it 'should set consult' do
      consult = 'how many working hours are per week?'
      ja = described_class.create_for_consult('test@test.com', job_offer, consult)
      expect(ja.consult).to eq(consult)
    end
  end

  describe 'process' do
    it 'should deliver contact info notification' do
      ja = described_class.create_for_consult('applicant@test.com', job_offer, 'how many working hours are per week?')
      expect(JobVacancy::App).to receive(:deliver).with(:notification, :contact_consult_email, ja)
      ja.process
    end
  end
end
