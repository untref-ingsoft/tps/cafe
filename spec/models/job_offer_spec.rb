require 'spec_helper'

describe JobOffer do
  subject(:job_offer) { described_class.new({ title: 'title' }) }

  describe 'model' do
    it { is_expected.to respond_to(:remuneration) }
    it { is_expected.to respond_to(:applications) }
    it { is_expected.to respond_to(:tags) }
  end

  describe 'valid?' do
    it 'should be invalid when title is blank' do
      check_validation(:title, "Title can't be blank") do
        described_class.new(location: 'a location')
      end
    end

    it 'should be valid when title is not blank' do
      job_offer = described_class.new(title: 'a title')
      expect(job_offer).to be_valid
    end

    it 'should be valid when remuneration is USD 1800' do
      job_offer = described_class.new(title: 'a title', remuneration: 'USD 1800')
      expect(job_offer).to be_valid
    end
  end

  describe 'tags valid?' do
    it 'should be valid when there are no tags' do
      job_offer = described_class.new(title: 'Title', tags: '')
      expect(job_offer).to be_valid
    end

    it 'should be valid when there are tags java, ruby' do
      job_offer = described_class.new(title: 'Title', tags: 'java, ruby')
      expect(job_offer.tags).to eq %w[java ruby]
    end

    it 'should be valid when there are different tags, not normalized' do
      job_offer = described_class.new(title: 'Title', tags: 'RUBY, dev , Junior ')
      expect(job_offer.tags).to eq %w[ruby dev junior]
    end

    it 'repeated tags' do
      job_offer = described_class.new(title: 'Title', tags: 'JAVA, java , java ')
      expect(job_offer.tags).to eq %w[java]
    end

    it 'intermediate spaces inside tags' do
      job_offer = described_class.new(title: 'Title', tags: 'java, java    dev,scala')
      expect(job_offer.tags).to eq ['java', 'java dev', 'scala']
    end

    it 'intermediate, trailing, and leading spaces inside tags' do
      job_offer = described_class.new(title: 'Title', tags: '  ruby dev, buenos    aires, senior    ')
      expect(job_offer.tags).to eq ['ruby dev', 'buenos aires', 'senior']
    end

    it 'repeated tags count as 1' do
      job_offer = described_class.new(title: 'Title', tags: 'ruby, Ruby, RUBY, junior')
      expect(job_offer.tags).to eq %w[ruby junior]
    end

    it 'cant input more than 3 tags' do
      check_validation(:tags, 'Tags Up to 3 tags are allowed') do
        described_class.new(title: 'Title', tags: 'ruby, dev, senior, buenos aires')
      end
    end
  end
end
