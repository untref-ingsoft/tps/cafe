require 'spec_helper'

describe User do
  subject(:user) { described_class.new({}) }

  describe 'model' do
    it { is_expected.to respond_to(:id) }
    it { is_expected.to respond_to(:name) }
    it { is_expected.to respond_to(:crypted_password) }
    it { is_expected.to respond_to(:email) }
    it { is_expected.to respond_to(:job_offers) }
  end

  describe 'valid?' do
    it 'should be false when name is blank' do
      user = described_class.new(email: 'john.doe@someplace.com',
                                 password: 'a_secure_passWord!')
      expect(user.valid?).to eq false
      expect(user.errors).to have_key(:name)
    end

    it 'should be false when email is not valid' do
      user = described_class.new(name: 'John Doe', email: 'john',
                                 password: 'a_secure_passWord!')
      expect(user.valid?).to eq false
      expect(user.errors).to have_key(:email)
    end

    it 'should be false when password is blank' do
      user = described_class.new(name: 'John Doe', email: 'john', password: '')
      expect(user.valid?).to eq false
    end

    it 'should be true when all field are valid' do
      user = described_class.new(name: 'John Doe', email: 'john@doe.com',
                                 password: 'a_secure_pasSWord1!')
      expect(user.valid?).to eq true
    end

    it 'should be false when password is shorter than 10 characters' do
      user = described_class.new(name: 'John Doe', email: 'john@doe.com',
                                 password: 'java')
      expect(user.valid?).to eq false
    end

    it 'should be false when password does not have lowercase letters' do
      user = described_class.new(name: 'John Doe', email: 'john@doe.com',
                                 password: '12345678910')
      expect(user.valid?).to eq false
    end

    it 'should be false when password does not have uppercase letters' do
      user = described_class.new(name: 'John Doe', email: 'john@doe.com',
                                 password: '12345acbdefg')
      expect(user.valid?).to eq false
    end

    it 'should be false when password does not have at least one number' do
      user = described_class.new(name: 'John Doe', email: 'john@doe.com',
                                 password: 'abcdFghiJkL')
      expect(user.valid?).to eq false
    end

    it 'should be false when password does not have at least one special character' do
      user = described_class.new(name: 'John Doe', email: 'john@doe.com',
                                 password: 'abcdFghiJk1')
      expect(user.valid?).to eq false
    end

    it 'should be false when password has consecutive repeating characters' do
      user = described_class.new(name: 'John Doe', email: 'john@doe.com',
                                 password: 'aabcdFghiJk1')
      expect(user.valid?).to eq false
    end
  end

  describe 'has password?' do
    let(:password) { 'password' }
    let(:user) do
      described_class.new(password: password,
                          email: 'john.doe@someplace.com',
                          name: 'john doe')
    end

    it 'should return false when password do not match' do
      expect(user).not_to have_password('invalid')
    end

    it 'should return true when password do  match' do
      expect(user).to have_password(password)
    end
  end
end
