Feature: Offer information

Scenario: the offer information is shown in the apply view
  Given a offer with title "Java dev", description "Junior dev wanted" and location "Buenos Aires" exists
  When an applicant applies to this offer
  Then he will see the information "Java dev" as title
  And "Junior dev wanted" as description
  And "Buenos Aires" as location

Scenario: some fields are not specified
  Given a offer with title "Java dev"
  When an applicant applies to this offer
  Then he will see the information "Java dev" as title
  And "---" as description
  And "---" as location
