Feature: Query about offers

Background:
    Given I am logged in as job offerer
    And Only a "Web Programmer" offer exists in the offers list

Scenario: Query mail sent
    Given I access the offer list page
    And I request information to the  Web Programmer offer
    When I ask 'how many working hours are per week?' 
    And enter the 'test@test.com' email address
    Then a mail with my question is sent to the offerer

Scenario: Query mail not sent, invalid sender email
    Given I access the offer list page
    And I request information to the  Web Programmer offer
    When I ask 'how many working hours are per week?' 
    And enter the 'test.com' email address
    Then a mail with my question is not sent to the offerer

Scenario: Query mail not sent, blank text
    Given I access the offer list page
    And I request information to the  Web Programmer offer
    When I ask '' 
    And enter the 'test@test.com' email address
    Then a mail with my question is not sent to the offerer

