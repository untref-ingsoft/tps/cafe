Feature: Captcha in sign up

Scenario: Captcha solved
    When the captcha is resolved
    Then the user is registered

Scenario: Captcha failed
    When the captcha is not resolved
    Then the user is not registered