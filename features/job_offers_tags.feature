Feature: Job offers tags

Background:
  Given I am logged in as job offerer

 Scenario: no tags
	  When I submit an offer with no tags
	  Then The offer is created with no tags
  
  Scenario: different tags, already normalized
	  When I submit an offer with tags "ruby, dev, junior"
	  Then The offer is created with tags "ruby", "dev" and "junior"
  
  Scenario: different tags, not normalized
	  When I submit an offer with tags "RUBY, dev , Junior "
	  Then The offer is created with tags "ruby", "dev" and "junior"
  
  Scenario: repeated tags, not normalized
    When I submit an offer with tags "JAVA, Java, java"
    Then The offer is created with tags "java"

  Scenario: tags with multiple consecutive withespaces
    When I submit an offer with tags "ruby   dev, buenos  aires"
    Then The offer is created with tags "ruby dev" and "buenos aires"
 
  Scenario: tags with trailing, leading and multiple consecutive withespaces
    When I submit an offer with tags "  ruby dev, buenos    aires, senior    "
    Then The offer is created with tags "ruby dev", "buenos aires" and "senior"
 
  Scenario: looks like more than 3 tags, but some are repeated
    When I submit an offer with tags "ruby, Ruby, RUBY, junior"
    Then The offer is created with tags "ruby" and "junior"
       
  Scenario: more than 3 different tags
    When I submit an offer with tags "ruby, dev, senior, buenos aires"
    Then The offer is not created

