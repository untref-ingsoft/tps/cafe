Given('Only a {string} offer exists in the offers list') do |title|
  @job_offer = JobOffer.new(title: title, description: '+5 exp')
  @job_offer.owner = UserRepository.new.first
  @job_offer.is_active = true
  JobOfferRepository.new.save @job_offer
end

Given('I access the offer list page') do
  visit '/job_offers'
end

Given('I request information to the  Web Programmer offer') do
  click_link 'Consult'
end

When('I ask {string}') do |consult|
  fill_in('job_offer_form[consult]', with: consult)
  @consult = consult
end

When('enter the {string} email address') do |email|
  fill_in('job_offer_form[applicant_email]', with: email)
  @email = email
end

Then('a mail with my question is sent to the offerer') do
  click_button('Send')
  @job_consult = JobConsult.create_for_consult(@email, @job_offer, @consult)
  mail_store = "#{Padrino.root}/tmp/emails"
  file = File.open("#{mail_store}/offerer@test.com", 'r')
  content = file.read
  content.include?(@job_offer.title).should be true
  content.include?(@job_offer.description).should be true
  content.include?(@job_offer.owner.email).should be true
  content.include?(@job_consult.consult).should be true
  content.include?(@job_consult.applicant_email).should be true
end

Then('a mail with my question is not sent to the offerer') do
  click_button('Send')
  page.should have_content('Please review the errors')
  File.exist?('offerer@test.com').should be false
end
