When('The captcha is resolved') do
  Recaptcha.configuration.skip_verify_env << 'test'
  visit '/job_offers'
  click_link 'Apply'
  fill_in('job_application_form[applicant_email]', with: 'applicant@test.com')
end

When('The captcha is not resolved') do
  Recaptcha.configuration.skip_verify_env.delete('test')
  visit '/job_offers'
  click_link 'Apply'
  fill_in('job_application_form[applicant_email]', with: 'applicant@test.com')
end

Then('apply the offer') do
  click_button('Apply')
  should have_content('Contact information sent')
end

Then("don't apply the offer") do
  click_button('Apply')
  should have_content('Invalid captcha')
end
