When('I submit an offer with no tags') do
  visit '/job_offers/new'
  fill_in('job_offer_form[title]', with: 'New Title')
  fill_in('job_offer_form[tags]', with: '')
  click_button('Create')
end

Then('The offer is created with no tags') do
  page.should have_content(OFFER_CREATED_MESSAGE)
end

When('I submit an offer with tags {string}') do |tags|
  visit '/job_offers/new'
  fill_in('job_offer_form[title]', with: 'New Title')
  fill_in('job_offer_form[tags]', with: tags)
  click_button('Create')
end

Then('The offer is created with tags {string}, {string} and {string}') do |tag1, tag2, tag3|
  within all('td')[6] do
    should have_content(tag1)
    should have_content(tag2)
    should have_content(tag3)
  end
end

Then('The offer is created with tags {string}') do |tag|
  within all('td')[6] do
    should have_content(tag)
  end
end

Then('The offer is created with tags {string} and {string}') do |tag1, tag2|
  within all('td')[6] do
    should have_content(tag1)
    should have_content(tag2)
  end
end

Then('The offer is not created') do
  page.should have_content('Up to 3 tags are allowed')
end
