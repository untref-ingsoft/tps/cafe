Given('I write {string} in the name field and {string} in the email field of the register form') do |name, email|
  visit '/register'
  fill_in('user[name]', with: name)
  fill_in('user[email]', with: email)
end

Given('I write {string} in the password and confirmation password field') do |password|
  fill_in('user[password]', with: password)
  fill_in('user[password_confirmation]', with: password)
end

When('I create a new user') do
  click_button('Create')
end

Then('I should see {string}') do |content|
  should have_content(content)
end
