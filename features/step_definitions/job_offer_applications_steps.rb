Given('I have an offer with {int} applications') do |applications|
  @job_offer = JobOffer.new(title: 'Ruby developer', location: 'Buenos Aires', description: '+5 exp',
                            remuneration: '100000', applications: applications)
  @job_offer.owner = UserRepository.new.first
  @job_offer.is_active = true

  JobOfferRepository.new.save @job_offer
end

When('I apply to this offer') do
  Recaptcha.configuration.skip_verify_env << 'test'
  visit '/job_offers'
  click_link 'Apply'
  fill_in('job_application_form[applicant_email]', with: 'applicant@test.com')
  click_button('Apply')
end

Then('the amount of applications to this offer in my list is {int}') do |applications|
  visit '/job_offers/my'
  within all('td')[5] do
    should have_content(applications)
  end
end
