Given('exist an offer {string} with tag {string} and {string}') do |title, tag1, tag2|
  @job_offer = JobOffer.new(title: title, location: 'a nice job', description: 'a nice job', remuneration: '100000',
                            tags: "#{tag1},#{tag2}")
  @job_offer.owner = UserRepository.new.first
  @job_offer.is_active = true

  JobOfferRepository.new.save @job_offer
end

Given('exist an offer {string} without tags') do |title|
  @job_offer = JobOffer.new(title: title, location: 'a nice job', description: 'a nice job', remuneration: '100000')
  @job_offer.owner = UserRepository.new.first
  @job_offer.is_active = true

  JobOfferRepository.new.save @job_offer
end

When('I apply for the {string} offer') do |title|
  visit '/job_offers'
  td = find('td', text: title)
  td.find(:xpath, '../td[5]/a[1]').click
end

Then('I should see the suggestion {string}') do |title|
  page.should have_content('Suggested offers')
  page.should have_content(title)
end

Then('I should not see the suggestion {string}') do |title|
  page.should_not have_content(title)
end

Then('I should not see any suggestions') do
  page.should have_content('No suggested offers found')
end
