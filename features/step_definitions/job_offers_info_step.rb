Given('a offer with title {string}, description {string} and location {string} exists') do |title, description, location|
  @job_offer = JobOffer.new(title: title, location: location, description: description)
  @job_offer.owner = UserRepository.new.first
  @job_offer.is_active = true

  JobOfferRepository.new.save @job_offer
end

Given('a offer with title {string}') do |title|
  @job_offer = JobOffer.new(title: title)
  @job_offer.owner = UserRepository.new.first
  @job_offer.is_active = true
  puts @job_offer.description.class
  JobOfferRepository.new.save @job_offer
end

When('an applicant applies to this offer') do
  visit '/job_offers'
  click_link 'Apply'
end

Then('he will see the information {string} as title') do |title|
  within('.job_offer_information__list') do
    within all('li')[0] do
      should have_content("Title: #{title}")
    end
  end
end

Then('{string} as description') do |description|
  within('.job_offer_information__list') do
    within all('li')[1] do
      should have_content("Description: #{description}")
    end
  end
end

Then('{string} as location') do |location|
  within('.job_offer_information__list') do
    within all('li')[2] do
      should have_content("Location: #{location}")
    end
  end
end
