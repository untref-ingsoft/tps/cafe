Given('I have a list with {string} and {string} offers') do |string, string2|
  visit '/job_offers/new'
  fill_in('job_offer_form[title]', with: string)
  click_button('Create')
  click_button('activate')
  visit '/job_offers/new'
  fill_in('job_offer_form[title]', with: string2)
  click_button('Create')
  click_button('activate')
end

Given('I have to list of offers filtered for {string}') do |string|
  visit '/job_offers/latest'
  fill_in('q', with: string)
  click_button('search')
end

When('I play button clear') do
  click_button('clear')
end

Then('I should see a list of offers with {string} and {string} as title') do |string, string2|
  page.should have_content(string)
  page.should have_content(string2)
end
