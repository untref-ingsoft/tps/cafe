When('the captcha is resolved') do
  visit '/register'
  fill_in('user[name]', with: 'john')
  fill_in('user[email]', with: 'john@mail.com')
  fill_in('user[password]', with: 'Abcd!12345678')
  fill_in('user[password_confirmation]', with: 'Abcd!12345678')
end

Then('the user is registered') do
  click_button('Create')
  should have_content('User created')
end

When('the captcha is not resolved') do
  Recaptcha.configuration.skip_verify_env.delete('test')
  visit '/register'
  fill_in('user[name]', with: 'john')
  fill_in('user[email]', with: 'john@mail.com')
  fill_in('user[password]', with: 'Abcd!12345678')
  fill_in('user[password_confirmation]', with: 'Abcd!12345678')
end

Then('the user is not registered') do
  click_button('Create')
  should have_content('Invalid captcha')
  Recaptcha.configuration.skip_verify_env << 'test'
end
