Given('An offer with the title {string}') do |title|
  @job_offer = JobOffer.new(title: title)
  @job_offer.owner = UserRepository.new.first
  @job_offer.is_active = true

  JobOfferRepository.new.save @job_offer
end

When('Exists applications in the offer {string}') do |title|
  visit '/job_offers'
  td = find('td', text: title)
  td.find(:xpath, '../td[5]/a[1]').click
  fill_in('job_application_form[applicant_email]', with: 'applicant@test.com')
  click_button('Apply')
  visit '/job_offers/my'
end

Then("I can't delete the offer") do
  apply_btn = find('button', text: 'Delete')
  expect(apply_btn.disabled?).to be(true)
end

When('Not exist applications in the {string} offer') do |_string|
  visit '/job_offers/my'
end

Then('I can remove the offer') do
  apply_btn = find('button', text: 'Delete')
  expect(apply_btn.disabled?).to be(false)
end
