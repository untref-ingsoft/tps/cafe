Feature: Strong Password Required
  As a job offerer
  I want to register in JobVacancy
  Background:
    Given I write "Johndoe" in the name field and "johndoe@mail.com" in the email field of the register form
    
  Scenario: Invalid short password
     Given I write "java" in the password and confirmation password field
     When I create a new user
     Then I should see 'Password should be at least 10 characters long'

  Scenario: No lowercase letters password
    Given I write "1234567890" in the password and confirmation password field
    When I create a new user
    Then I should see 'Password should be at least one lowercase letter'

  Scenario: No uppercase letters in password
    Given I write "johnny2021" in the password and confirmation password field
    When I create a new user
    Then I should see 'Password should be at least one uppercase letter'

  Scenario: No number password
    Given I write "abcdFghiJkL" in the password and confirmation password field
    When I create a new user
    Then I should see 'Password should have at least one number'

  Scenario: no special character in password
    Given I write "JohNny2021" in the password and confirmation password field
    When I create a new user
    Then I should see 'Password should have at least one special character'

  Scenario: Consecutive duplicated characters password
    Given I write "Johnny2021!" in the password and confirmation password field
    When I create a new user
    Then I should see 'Password should not have repeating consecutive characters'

   Scenario: Valid password
     Given I write "John2021!?" in the password and confirmation password field
     When I create a new user
     Then I should see 'User created'
    


  
