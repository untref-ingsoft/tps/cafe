Feature: Job Offers List

   Background:
  	   Given I am logged in as job offerer
  	   And I have a list with "Java developer" and "Ruby developer" offers
  
   Scenario: Clear button cleans the search
      Given I have to list of offers filtered for "java"
      When I play button clear
      Then I should see a list of offers with "Ruby developer" and "Java developer" as title
  
   Scenario: Clear button cleans the search
      Given I have to list of offers filtered for ""
      When I play button clear
      Then I should see a list of offers with "Ruby developer" and "Java developer" as title

