Feature: Amount of applications

Background:
      Given I am logged in as job offerer

    Scenario: a successful application increases the amount
      Given I have an offer with 0 applications
      When I apply to this offer
      Then the amount of applications to this offer in my list is 1