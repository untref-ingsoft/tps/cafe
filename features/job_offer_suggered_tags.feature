Feature: Offer Suggestions

Background: Background name
    Given exist an offer "Java Programmer" with tag "java" and "junior"
    Given exist an offer "Ruby Developer" with tag "ruby" and "ssr"
    Given exist an offer "Java Architect" with tag "java" and "senior"
    Given exist an offer "Python Tester" without tags

Scenario: Apply to job offer with match tags
    When I apply for the "Java Programmer" offer 
    Then I should see the suggestion "Java Architect" 
    And I should not see the suggestion "Ruby Developer" 
    And I should not see the suggestion "Python Tester"

Scenario: Apply to job offer without match tags
    When I apply for the "Ruby Developer" offer
    Then I should not see any suggestions

Scenario: Apply to job offer without tags
    When I apply for the "Python Tester" offer
    Then I should not see any suggestions