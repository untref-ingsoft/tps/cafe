Feature: Prevent delete offer

Background:
    Given I am logged in as job offerer
    And An offer with the title "Java dev"

Scenario: is not allowed to delete offers with applications
    When Exists applications in the offer "Java dev"
    Then I can't delete the offer

Scenario: allow to remove offers without applications
    When Not exist applications in the "Java dev" offer
    Then I can remove the offer