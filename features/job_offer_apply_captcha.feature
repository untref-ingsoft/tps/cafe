Feature: Captcha in apply

Background: 
    Given An offer with the title "Java dev"

Scenario: Captcha solved
    When The captcha is resolved
    Then apply the offer

Scenario: Captcha failed
    When The captcha is not resolved
    Then don't apply the offer